#Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import plotly.graph_objs as go
import numpy as np
import xpresso.ai.core.data.visualization.utils as utils
from xpresso.ai.core.data.visualization.plotly.res.plot_types import diagram

#End Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------

class NewPlot(diagram):
    def __init__(self, input_1, input_2, plot_title = None, zero_filter = False,
                 output_format = utils.HTML, output_path = None, auto_render = False, filename=None):

        #Add Style to Plot
        self.style_plot()

        if plot_title is not None or filename is not None:
            self.set_labels_manual(plot_label=plot_title, filename=filename)

        #Convert Inputs to Lists
        input_1 = (np.array(input_1).tolist())
        input_2 = (np.array(input_2).tolist())

        #Check Input Types
        if (type(input_1) is dict and type(input_2) is str):
            self.labels = []
            self.values = []

            #Convert Dict to Lists
            for key in input_1:
                if (input_2 == "KEY_L"):
                    self.labels.append(key)
                    self.values.append(input_1[key])
                elif (input_2 == "KEY_V"):
                    self.labels.append(input_1[key])
                    self.values.append(key)
                else:
                    #Return Error on Invalid Conversion Flag
                    print("ERROR: Invalid conversion flag!")
                    utils.halt()

            #Process Filter Argument
            self.filter_data(zero_filter, self.labels, self.values)

            #Process Set Arguments
            if (not(output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format = output_format,auto_open = True,
                            output_path=output_path)
        elif (type(input_1) is list and type(input_2) is list):
            #Set List Values
            self.labels = input_1
            self.values = input_2

            #Return Error on Differing List Lengths
            if (not((len(self.labels) == len(self.values)))):
                print("ERROR: Differing list lengths!")
                utils.halt()

            #Process Filter Argument
            self.filter_data(zero_filter, self.labels, self.values)

            #Process Set Arguments
            if (not(output_path is None)):
                self.output_path = output_path

            if (auto_render):
                self.render(output_format = output_format,auto_open = True,
                            output_path=output_path)
        else:
            #Return Error on Invalid Type
            print("ERROR: Invalid Input Type!")
            utils.halt()

    #End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------
    def style_plot(self):
        #Apply Default Style to Plot
        self.apply_default_style()

        #Apply Custom Style to Plot
        self.marker = None

    def process(self,output_format=None):
        #Generate and Return Plot Config
        plot_data = go.Pie(labels = self.labels, values = self.values,
                           hovertemplate = self.hover_tool, name =
                           self.graph_name)

        plot_layout = go.Layout(autosize = True, width =self.plot_width,
                                height = self.plot_height, paper_bgcolor = str(self.bg_color), plot_bgcolor = str(self.plot_color),
                                title = self.plot_title, xaxis = self.x_axis_title, yaxis = self.y_axis_title,
                                margin = go.layout.Margin(l = int(self.left), r = int(self.right), b = int(self.bottom), t = int(self.top), pad = int(self.padding)))

        return {'data': [plot_data], 'layout': plot_layout}

    def render(self, output_format = utils.HTML, output_path = None, auto_open = True):
        self._render_single_plot(__name__, output_format = output_format, output_path = output_path, auto_open = auto_open)

    #End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------

#End Graph Class---------------------------------------------------------------------------------------------------------------------------------------------------------------
